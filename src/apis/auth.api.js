const { Router } = require('express');
// const userRepo = require('../repository/users.mongo-repo');
const passport = require('passport');
const userService = require('../services/users.service');
const authService = require('../services/auth.service');

const loginFormController = (req, resp) => {
  const error = req.flash('error');
  const infoMessage = req.flash('info');
  resp.render('login', {
    error,
    title: 'Login',
    message: infoMessage,
  });
};

const registerFormController = (req, resp) => {
  const payload = {
    title: 'Register',
    error: undefined,
  };
  const errors = req.flash('register_error');
  if (errors && errors.length !== 0) {
    payload.error = errors[0];
  }
  return resp.render('register', payload);
};

const loginController = async (req, resp) => {
  const loginForm = req.body;
  console.log(loginForm);
  try {
    const userInfo = await authService.login(loginForm.username, loginForm.password);
    console.log(userInfo);
    resp.redirect('/');
  } catch (err) {
    return resp.redirect('/login');
  }
};
const registerController = async (req, resp) => {
  const userForm = {
    username: req.body.username,
    email: req.body.email,
    password: req.body.password,
    confirmPassword: req.body['confirm-password'],
  };
  console.log(userForm);
  try {
    const userInserted = await userService.createUser(userForm);
    resp.redirect('/login');
  } catch (err) {
    req.flash('register_error', err);
    return resp.redirect('/register');
  }
};
const createAuthRoutes = () => {
  const authRoutes = Router();
  authRoutes.get('/login', loginFormController);
  authRoutes.get('/register', registerFormController);
  authRoutes.post('/login', passport.authenticate('local', { failureRedirect: '/login' }), loginController);
  authRoutes.post('/register', registerController);
  return authRoutes;
};

const authRoutes = createAuthRoutes();

module.exports = authRoutes;
